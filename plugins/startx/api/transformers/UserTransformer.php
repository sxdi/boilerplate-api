<?php namespace Startx\Api\Transformers;

use Startx\User\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    // Related transformer that can be included
    public $availableIncludes = [
        'workshop',
        'branch',
        'permission',
        'access_workshop'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'        => $user->id,
            'phone'     => $user->phone,
            'parameter' => $user->parameter,
        ];
    }

    public function includeWorkshop(User $user)
    {
        $workshop = \Otohub\Workshop\Models\Detail::whereUserId($user->id)->first();
        if($workshop) {
            $workshop = \Otohub\Workshop\Models\Workshop::whereId($workshop->workshop_id)->first();
            return $this->item($workshop, new \Startx\Api\Transformers\WorkshopTransformer);
        }
    }

    public function includeBranch(User $user)
    {
        $accesses = \Otohub\Workshop\Models\Access::whereUserId($user->id);
        $accTrans = new \Startx\Api\Transformers\WorkshopAccessTransformer;
        return $this->primitive([
            'data' => [
                'string'   => implode(',', $accesses->pluck('branch_id')->toArray()),
                'array'    => $accesses->pluck('branch_id'),
                'branches' => $accTrans->call($accesses->get())
            ]
        ]);
    }

    public function includePermission(User $user)
    {
        $roleIds       = \Otohub\Workshop\Models\Access::whereUserId($user->id)->pluck('role_id');
        $permissionIds = \Startx\System\Models\RolePermission::whereIn('role_id', $roleIds)->pluck('permission_id');
        $permissions   = \Startx\System\Models\Permission::whereIn('id', $permissionIds);
        return $this->primitive([
            'data' => [
                'string' => implode(',', $permissions->pluck('code')->toArray()),
                'array'  => $permissions->pluck('code'),
            ]
        ]);
    }

    public function includeAccessWorkshop(User $user)
    {
        $access = \Otohub\Workshop\Models\Access::whereUserId($user->id)->get();
        return $this->collection($access, new \Startx\Api\Transformers\WorkshopAccessTransformer);
    }
}
