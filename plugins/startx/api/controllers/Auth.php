<?php namespace Startx\Api\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Octobro\API\Classes\ApiController;

class Auth extends ApiController
{

    public function request()
    {
        $rules = [
            'phone' => 'required|numeric',
        ];

        $attributeNames = [
            'phone' => 'whatsapp'
        ];

        $validator = \Validator::make(post(), $rules, [], $attributeNames);
        if($validator->fails()) {
            return response()->json([
                'result' => false,
                'message'=> $validator->messages()->first()
            ]);
        }

        /**
         * Generate Code
        */
        $code     = new \Startx\Core\Classes\Generator;
        $code     = $code->makeNumeric();

        /**
         * Send Whatsapp
        */
        $isActive = \Startx\Auth\Models\Request::wherePhone(post('phone'))->whereStatus('active')->first();
        if($isActive) {
            $code = $isActive->code;
        }
        else {
            $req  = \Startx\Auth\Models\Request::firstOrNew([
                'phone' => post('phone'),
                'code'  => $code
            ]);
            $req->save();
        }


        /**
         * Send Whatsapp
        */
        $msg = 'Berikut ini adalah kode masuk anda *'.$code. '* dengan nomor ponsel (hanya berlaku 5 menit).'."\n";
        $msg .= '*PENTING: Mohon tidak menyebarkan kode ke orang lain atau pihak OtoHub Indonesia, demi keamanan akun.*';
        $wa  = new \Startx\Core\Classes\Whatsapp;
        $wa->send(post('phone'), $msg);

        return response()->json([
            'result' => true,
            'message'=> 'Kode masuk berhasil dibuat dan terkirim'
        ]);
    }

    public function login()
    {
        $rules = [
            'phone' => 'required|numeric',
            'code'  => 'required'
        ];

        $attributeNames = [
            'phone' => 'whatsapp',
            'code'  => 'kode'
        ];

        $validator = \Validator::make(post(), $rules, [], $attributeNames);
        if($validator->fails()) {
            return response()->json([
                'result' => false,
                'message'=> $validator->messages()->first()
            ]);
        }

        // try {
        //     if (!$token = JWTAuth::attempt(post())) {
        //         return response()->json(['error' => 'invalid_credentials'], 400);
        //     }
        // } catch (JWTException $e) {
        //     return response()->json(['error' => 'could_not_create_token'], 500);
        // }
        // return response()->json(['message' => $token]);

        $req  = \Startx\Auth\Models\Request::wherePhone(post('phone'))->whereCode(post('code'))->whereStatus('active')->first();
        if(!$req) {
            return response()->json([
                'result' => false,
                'message'=> 'Pengguna atau kode tidak dapat ditemukan'
            ]);
        }

        \Startx\Auth\Models\Request::wherePhone($req->phone)->update([
            'status' => 'used'
        ]);

        $user = \Startx\User\Models\User::firstOrNew([
            'phone' => post('phone')
        ]);
        $user->save();

        return response()->json([
            'result'  => true,
            'response'=> $this->respondWithItem($user, new \Startx\Api\Transformers\UserTransformer)
        ]);
    }
}
