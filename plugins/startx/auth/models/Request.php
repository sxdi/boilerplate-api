<?php namespace Startx\Auth\Models;

use Model;

/**
 * Request Model
 */
class Request extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'startx_auth_requests';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['phone', 'code', 'status'];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'expired_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne         = [];
    public $hasMany        = [];
    public $hasOneThrough  = [];
    public $hasManyThrough = [];
    public $belongsTo      = [];
    public $belongsToMany  = [];
    public $morphTo        = [];
    public $morphOne       = [];
    public $morphMany      = [];
    public $attachOne      = [];
    public $attachMany     = [];

    public function beforeCreate()
    {
        $this->expired_at = \Carbon\Carbon::parse($this->created_at)->addMinutes(5);
    }
}
