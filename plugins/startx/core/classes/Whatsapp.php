<?php namespace Startx\Core\Classes;

/**
 *
 */
class Whatsapp
{
    public function reformat($receiver)
    {
        $code = '08';
        $intr = '628';
        $peach= substr($receiver, 0, 2);

        if($peach == $code) {
            $receiver = str_replace($code, $intr, $receiver);
        }

        return $receiver;
    }

	public function send($receiver, $msg)
	{
        $message  = 'OTOHUB INDONESIA — '.$msg;
        $data     = [
            'text'     => $message,
            'number'   => $this->reformat($receiver),
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('APP_WHATSAPP').'?'.http_build_query($data));
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
	}
}
