<?php

Route::get('/', function() {
    return;
});

Route::group([
	'prefix'     => 'api/v1',
	'namespace'  => 'Startx\Api\Controllers',
    'middleware' => 'cors'
], function() {

    Route::group(['prefix'=> 'customer'], function() {
        Route::get('/', 'Customer@get');
        Route::post('save', 'Customer@save');

        Route::group(['prefix'=> 'vehicle'], function() {
            Route::get('/', 'CustomerVehicle@get');
        });
    });

    Route::group(['prefix' => 'account'], function() {

        /**
         * Workshop Account
        */
        Route::group(['prefix' => 'workshop'], function() {
            Route::get('/', 'AccountWorkshop@get');
            Route::get('detail', 'AccountWorkshop@detail');

            /**
             * Setup
            */
            Route::group(['prefix' => 'setup'], function() {
                Route::get('size', 'AccountWorkshopSetup@size');
                Route::post('owner', 'AccountWorkshopSetup@owner');
                Route::post('workshop', 'AccountWorkshopSetup@workshop');
                Route::post('finish', 'AccountWorkshopSetup@finish');
            });

            /**
             * Order
            */
            Route::group(['prefix' => 'order'], function() {
                Route::get('/', 'AccountWorkshopOrder@get');
                Route::get('/detail', 'AccountWorkshopOrder@detail');
                Route::get('/parameter', 'AccountWorkshopOrder@parameter');
                Route::post('/save', 'AccountWorkshopOrder@save');
                Route::post('/picture', 'AccountWorkshopOrder@picture');
            });

            /**
             * Service
            */
            Route::group(['prefix' => 'service'], function() {
                Route::get('/', 'AccountWorkshopService@get');
                Route::get('/detail', 'AccountWorkshopService@detail');
                Route::get('/parameter', 'AccountWorkshopService@parameter');
                Route::post('/save', 'AccountWorkshopService@save');
                Route::post('/picture', 'AccountWorkshopService@picture');
            });

            /**
             * Product
             */
            Route::group(['prefix' => 'product'], function() {
                Route::get('/', 'AccountWorkshopProduct@get');
                Route::get('/parameter', 'AccountWorkshopProduct@parameter');
                Route::post('/save', 'AccountWorkshopProduct@save');
                Route::post('/picture', 'AccountWorkshopProduct@picture');
            });

            /**
             * Employee
            */
            Route::group(['prefix' => 'employee'], function() {
                Route::get('/', 'AccountWorkshopEmployee@get');
                Route::get('/parameter', 'AccountWorkshopEmployee@parameter');
                Route::get('/delete', 'AccountWorkshopEmployee@delete');
                Route::post('/save', 'AccountWorkshopEmployee@save');
                Route::post('/picture', 'AccountWorkshopEmployee@picture');
            });

            /**
             * Customer
             */
            Route::group(['prefix' => 'customer'], function() {
                Route::get('/', 'AccountWorkshopCustomer@get');
                Route::get('/parameter', 'AccountWorkshopCustomer@parameter');
                Route::get('/delete', 'AccountWorkshopCustomer@delete');
                Route::post('/save', 'AccountWorkshopCustomer@save');
                Route::post('/picture', 'AccountWorkshopCustomer@picture');
            });

            /**
             * Branch
            */
            Route::group(['prefix' => 'branch'], function() {
                Route::get('/', 'AccountWorkshopBranch@get');
                Route::get('/detail', 'AccountWorkshopBranch@detail');
                Route::get('/parameter', 'AccountWorkshopBranch@parameter');
                Route::post('/save', 'AccountWorkshopBranch@save');
            });

            /**
             * Inventory
            */
            Route::group(['prefix' => 'inventory'], function() {
                Route::get('/', 'AccountWorkshopInventory@get');
                Route::get('/detail', 'AccountWorkshopInventory@detail');
                Route::get('/parameter', 'AccountWorkshopInventory@parameter');
                Route::post('/save', 'AccountWorkshopInventory@save');
            });
        });
    });

    Route::group(['prefix' => 'location'], function() {
        Route::get('province', 'Location@province');
        Route::get('regency', 'Location@regency');
        Route::get('district', 'Location@district');
        Route::get('village', 'Location@village');
    });

    Route::group(['prefix'=> 'auth'], function() {
        Route::post('request', 'Auth@request');
        Route::post('login', 'Auth@login');
    });
});
