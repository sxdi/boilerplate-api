<?php namespace Startx\Core\Classes;

/**
 *
 */
class Generator
{
	public function make()
	{
		return md5(\Hash::make('Y-m-d H:i:s').$this->makeNumeric());
	}

	public function makeNumeric()
	{
		return rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
	}
}
