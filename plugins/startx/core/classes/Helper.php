<?php namespace Startx\Core\Classes;

/**
 *
 */
class Helper
{
	public function invoiceCode()
	{
		$now  = date('Y-m-d');
		$month= \Carbon\Carbon::parse($now)->addMonths(1)->format('m');
		switch ($month) {
			case '01':
				$month = 'I';
				break;
			case '02':
				$month = 'II';
				break;
			case '03':
				$month = 'III';
				break;
			case '04':
				$month = 'IV';
				break;
			case '05':
				$month = 'V';
				break;
			case '06':
				$month = 'VI';
				break;
			case '07':
				$month = 'VII';
				break;
			case '08':
				$month = 'VIII';
				break;
			case '09':
				$month = 'IX';
				break;
			case '10':
				$month = 'X';
				break;
			case '11':
				$month = 'XI';
				break;
			case '12':
				$month = 'XII';
				break;
			default:
				break;
		}
		return 'INV/ARBAALIVING/'.$month.'/'.$this->randCode();
	}

	public function registerCode()
	{
		$now  = date('Y-m-d');
		$month= \Carbon\Carbon::parse($now)->format('m');
		switch ($month) {
			case '01':
				$month = 'I';
				break;
			case '02':
				$month = 'II';
				break;
			case '03':
				$month = 'III';
				break;
			case '04':
				$month = 'IV';
				break;
			case '05':
				$month = 'V';
				break;
			case '06':
				$month = 'VI';
				break;
			case '07':
				$month = 'VII';
				break;
			case '08':
				$month = 'VIII';
				break;
			case '09':
				$month = 'IX';
				break;
			case '10':
				$month = 'X';
				break;
			case '11':
				$month = 'XI';
				break;
			case '12':
				$month = 'XII';
				break;
			default:
				break;
		}
		return 'INTL/ARBAALIVING/'.$month.'/'.$this->randCode();
	}

	public function randCode()
	{
		return rand(0,9).rand(0,9).rand(0,9).rand(0,9);
	}

	public function stringToArray($string)
	{
		return explode(',', $string);
	}

	public function arrayToString($array)
	{
		return implode(',', $array);
	}

	public function transformDate($date)
	{
		if($date) {
			return [
				'ymd' 		=> \Carbon\Carbon::parse($date)->format('Y-m-d'),
				'period'    => \Carbon\Carbon::parse($date)->format('F Y'),
				'dFY' 		=> \Carbon\Carbon::parse($date)->format('d F Y'),
			];
		}

		return false;
	}

	public function transformProvince($province)
	{
		if($province) {
			return [
				'id' 	=> $province->id,
				'name' 	=> $province->name,
				'snake' => ucwords(strtolower($province->name)),
			];
		}
	}

	public function transformRegency($regency)
	{
		if($regency) {
			return [
				'id' 	=> $regency->id,
				'name' 	=> $regency->name,
				'snake' => ucwords(strtolower($regency->name)),
			];
		}
	}

	public function transformDistrict($district)
	{
		if($district) {
			return [
				'id' 	=> $district->id,
				'name' 	=> $district->name,
				'snake' => ucwords(strtolower($district->name)),
			];
		}
	}

	public function transformVillage($village)
	{
		if($village) {
			return [
				'id' 	=> $village->id,
				'name' 	=> $village->name,
				'snake' => ucwords(strtolower($village->name)),
			];
		}
	}

	public function transformPrice($value)
	{
		return [
			'text' => number_format($value),
			'value'=> (int) $value,
			'idr'  => 'Rp. '.number_format($value)
		];
	}
}
