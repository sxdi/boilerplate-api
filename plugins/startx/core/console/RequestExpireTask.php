<?php namespace Startx\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RequestExpireTask extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startx:expire-task';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $ss      = \Otohub\Location\Models\Province::get();
        foreach ($ss as $key => $s) {
            $s->id = ($key + 1);
            $s->save();
            $this->info('Inserting '. $key. ' data of '.count($ss));
        }
        return;

        $now     = date('Y-m-d H:i');
        $request = \Startx\Auth\Models\Request::whereDate('expired_at', 'like', '%'.$now.'%')->update([
            'status' => 'expired'
        ]);
        $message = 'Total kode '.count($request). ' telah dinon aktifkan';
        $this->info($message);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
