<?php namespace Startx\System\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRolePermissionsTable extends Migration
{
    public function up()
    {
        Schema::create('startx_system_role_permissions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('role_id');
            $table->integer('permission_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startx_system_role_permissions');
    }
}
