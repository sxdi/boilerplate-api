<?php namespace Startx\System\Classes;

/**
 *
 */
class SystemManager
{
	public function hasPermission($inPermissions)
	{
		$userId        = \Session::get('userLogin');
		$roleIds       = \Startx\User\Models\Role::whereUserId($userId)->pluck('role_id');
		$permissionIds = \Startx\System\Models\RolePermission::whereIn('role_id', $roleIds)->pluck('permission_id');
		$permissions   = \Startx\System\Models\Permission::whereIn('id', $permissionIds)->pluck('slug')->toArray();

		if (!in_array($inPermissions, $permissions)) {
			return false;
		}

		return true;
	}
}
