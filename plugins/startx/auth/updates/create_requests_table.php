<?php namespace Startx\Auth\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestsTable extends Migration
{
    public function up()
    {
        Schema::create('startx_auth_requests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('status', ['active', 'expired'])->default('active');
            $table->string('phone');
            $table->string('code');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('startx_auth_requests');
    }
}
